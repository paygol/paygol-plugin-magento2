<?php
\Magento\Framework\Component\ComponentRegistrar::register(
   \Magento\Framework\Component\ComponentRegistrar::MODULE,
   'Paygol_PaygolMagento',
   // -> Production mode
   //__DIR__
   // -> Develop mode
   isset($file) && realpath($file) == __FILE__ ? dirname($file) : __DIR__
);
