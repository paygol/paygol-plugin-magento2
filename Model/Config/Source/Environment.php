<?php

namespace Paygol\PaygolMagento\Model\Config\Source;

use Paygol\PaygolCore\EnvironmentEnum;

class Environment
{
   public function toOptionArray()
   {
      return [
         ['value' => EnvironmentEnum::DEVELOPMENT, 'label' => __('Development')],
         ['value' => EnvironmentEnum::PRODUCTION, 'label' => __('Production')],
      ];
   }
}
