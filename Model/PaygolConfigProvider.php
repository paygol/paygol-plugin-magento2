<?php

namespace Paygol\PaygolMagento\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

class PaygolConfigProvider implements ConfigProviderInterface
{

    /**
     * @var \Magento\Framework\View\Asset\Repository
     */
    protected $_assetRepo;

    public function __construct(
        \Magento\Framework\View\Asset\Repository $assetRepo
    )
    {
        $this->_assetRepo = $assetRepo;
    }

    public function getConfig()
    {
        return [
            'logoUrl' => $this->_assetRepo->getUrl("Paygol_PaygolMagento::images/LogoPaygol.png")
        ];
    }
}