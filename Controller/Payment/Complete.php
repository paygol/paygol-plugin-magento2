<?php

namespace Paygol\PaygolMagento\Controller\Payment;

use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Sales\Api\TransactionRepositoryInterface;
use Magento\Sales\Model\Order\Payment\Transaction;

use Psr\Log\LoggerInterface;

use Paygol\PaygolCore\PaygolApi;
use Paygol\PaygolCore\EnvironmentEnum;
use Paygol\PaygolCore\Models\Payer;
use Paygol\PaygolCore\Models\RedirectUrls;
use Paygol\PaygolCore\Exceptions\InvalidParameterException;

use Paygol\PaygolMagento\Model\Factory\Connector;

class Complete implements HttpGetActionInterface, CsrfAwareActionInterface
{
   const PGID_REQUEST = 'PGID';
   const STATUS_REQUEST = 'status';
   const CUSTOM_REQUEST = 'custom';
   const KEY_REQUEST = 'key';

   const CHECKOUT_ONEPAGE_FAILURE = 'checkout/onepage/failure';
   const CHECKOUT_ONEPAGE_SUCCESS = 'checkout/onepage/success';

   /**
    * @var \Magento\Framework\App\Config\ScopeConfigInterface
    */
   private $_scopeConfig;

   /**
    * @var \Psr\Log\LoggerInterface
    */
   private $_logger;

   /**
    * @var PaymentHelper
    */
   private $_paymentHelper;

   /**
    * @var \Magento\Sales\Api\TransactionRepositoryInterface
    */
   private $_transactionRepository;

   /**
    * @var \Paygol\PaygolMagento\Model\Factory\Connector
    */
   private $_tpConnector;

   /**
    * @var Magento\Framework\App\RequestInterface
    */
   private $_request;

   /**
    * @var MessageManagerInterface
    */
   private $_messageManager;

   /**
    * @var \Magento\Framework\Controller\ResultFactory
    */
   protected $_resultFactory;

   public function __construct(
      Connector $tpc,
      Context $context,
      ScopeConfigInterface $scopeConfig,
      LoggerInterface $logger,
      PaymentHelper $paymentHelper,
      TransactionRepositoryInterface $transactionRepository,
      RequestInterface $request,
      MessageManagerInterface $messageManager
   ) {
      $this->_scopeConfig = $scopeConfig;
      $this->_logger = $logger;
      $this->_paymentHelper = $paymentHelper;
      $this->_transactionRepository = $transactionRepository;
      $this->_tpConnector = $tpc;
      $this->_request = $request;
      $this->_messageManager = $messageManager;
      $this->_resultFactory = $context->getResultFactory();
   }

   public function execute()
   {
      $this->_logger->info('Paygol Full Turn!!!');
      $params = $this->_request->getParams();

      if (empty($params)) {
         exit();
      }

      $paygolTransactionId = $params[self::PGID_REQUEST];
      $paygolStatus = $params[self::STATUS_REQUEST];
      $orderIdEncoded = $params[self::CUSTOM_REQUEST];
      $signature = $params[self::KEY_REQUEST];
      $resultRedirect = $this->_resultFactory->create(ResultFactory::TYPE_REDIRECT);

      // Request string is not valid
      $isValid = $this->validateRequest($paygolTransactionId, $paygolStatus, $orderIdEncoded, $signature);
      if (!$isValid) {
         $this->_messageManager->addErrorMessage(
            __('An error occurred while validating response from selected payment method. Please contact to seller.', 'paygol-magento')
         );

         $resultRedirect->setPath(self::CHECKOUT_ONEPAGE_FAILURE);
         return $resultRedirect;
      }

      // Decode Order id
      $orderId = $this->base64_url_decode($orderIdEncoded);

      // Payment with error or was cancelled
      if ($paygolStatus !== 'completed') {
         $result = $this->redirectToErrorPage($orderId);
         $resultRedirect->setPath($result);
         return $resultRedirect;
      }

      // Payment Confirmation
      $result = $this->confirmTransacction($paygolTransactionId, $orderId);
      $resultRedirect->setPath($result);
      return $resultRedirect;
   }

   public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
   {
      return null;
   }

   public function validateForCsrf(RequestInterface $request): ?bool
   {
      return true;
   }

   // Method for request validation, based on the hash computation of part the request string.
   private function validateRequest($paygolTransactionId, $paygolStatus, $orderIdEncoded, $signature)
   {
      $secret = $this->_tpConnector->getTokenSecret();

      // Part of the request to be validated
      $query_string =
         self::PGID_REQUEST .
         '=' .
         $paygolTransactionId .
         '&' .
         self::STATUS_REQUEST .
         '=' .
         $paygolStatus .
         '&' .
         self::CUSTOM_REQUEST .
         '=' .
         $orderIdEncoded;

      // Compute signature for comparision
      $computed_signature = $this->computeSignature($query_string, $secret);

      // Request is valid if both signatures are equals.
      return $computed_signature === $signature;
   }

   // Method for confirm transaction (payment)
   private function confirmTransacction($paygolTransactionId, $orderId)
   {
      try {
         $this->_logger->info('Processing Paygol payment ' . $paygolTransactionId . ' for order nº ' . $orderId);

         // Validate payment against Paygol API
         $paygol = new PaygolApi(
            $this->_tpConnector->getTokenService(),
            $this->_tpConnector->getTokenSecret(),
            $this->_tpConnector->getEnviroment()
         );
         $response = $paygol->getPaymentStatus($paygolTransactionId);

         if (property_exists((object) $response, 'payment')) {
            // Successful validation, validate payment status
            if ($response['payment']['status'] === 'completed') {
               return $this->redirectToSuccessPage($orderId);
            } else {
               // Payment with error status.
               $this->_logger->error($response['payment']);
               return $this->redirectToErrorPage($orderId);
            }
         } else {
            // Failed validation
            $this->_logger->error($response['error']);
            return $this->redirectToErrorPage($orderId);
         }
      } catch (Exception $e) {
         $this->_logger->error($e->getMessage());
         return $this->redirectToErrorPage($orderId);
      }
   }

   private function displayError($message, $description = null)
   {
      $errMsg = '<p>' . $message . '</p>';
      if ($description) {
         $errMsg = $errMsg . '<p>' . $description . '</p>';
      }

      $this->_messageManager->addErrorMessage(__($errMsg, 'paygol-magento'), 'error');
   }

   private function redirectToSuccessPage($orderId)
   {
      // The order is marked as completed
      $status = \Magento\Sales\Model\Order::STATE_PROCESSING;
      $state = \Magento\Sales\Model\Order::STATE_PROCESSING;
      $message = __('Paygol approved', 'paygol-magento');
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      $order_model = $objectManager->get('Magento\Sales\Model\Order');
      $order = $order_model->load($orderId);

      $payment = $order->getPayment();
      $transaction = $this->_transactionRepository->getByTransactionType(
         Transaction::TYPE_ORDER,
         $payment->getId(),
         $payment->getOrder()->getId()
      );
      $payment->setIsTransactionPending(false);
      $payment->setIsTransactionApproved(true);
      $payment->setSkipOrderProcessing(true);
      $payment->addTransactionCommentsToOrder($transaction, $message);

      $invoice = $objectManager->create('Magento\Sales\Model\Service\InvoiceService')->prepareInvoice($order);
      $invoice = $invoice
         ->setTransactionId($payment->getTransactionId())
         ->addComment(__('Invoice created.', 'paygol-magento'))
         ->setRequestedCaptureCase(\Magento\Sales\Model\Order\Invoice::CAPTURE_ONLINE);
      $invoice->register()->pay();
      $invoice->save();

      // Save the invoice to the order
      $transactionInvoice = $objectManager
         ->create('Magento\Framework\DB\Transaction')
         ->addObject($invoice)
         ->addObject($invoice->getOrder());
      $transactionInvoice->save();

      $order->addStatusHistoryComment(__('Invoice #%1.', $invoice->getId()), 'paygol-magento')->setIsCustomerNotified(true);
      $order->setState($state)->setStatus($status);

      $transaction->save();
      $order->save();
      $payment->save();
      return self::CHECKOUT_ONEPAGE_SUCCESS;
   }

   private function redirectToErrorPage($orderId)
   {
      // The order is marked as failed
      $status = \Magento\Sales\Model\Order::STATE_CANCELED;
      $state = \Magento\Sales\Model\Order::STATE_CANCELED;
      $message = __('Payment from Paygol declined', 'paygol-magento');
      $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
      $order_model = $objectManager->get('Magento\Sales\Model\Order');
      $order = $order_model->load($orderId);

      $payment = $order->getPayment();
      $transaction = $this->_transactionRepository->getByTransactionType(
         Transaction::TYPE_ORDER,
         $payment->getId(),
         $payment->getOrder()->getId()
      );
      $payment->setIsTransactionDenied(true);
      $payment->setSkipOrderProcessing(true);
      $payment->addTransactionCommentsToOrder($transaction, $message);
      $order->setState($state)->setStatus($status);

      $payment->save();
      $transaction->save();
      $order->cancel();
      $order->save();

      $this->_messageManager->addErrorMessage(
         __('There is a problem with the selected payment method or it was cancelled. Please contact to seller.', 'paygol-magento')
      );
      return self::CHECKOUT_ONEPAGE_FAILURE;
   }

   private function base64_url_decode($b64text)
   {
      return base64_decode(strtr($b64text, '._-', '+/='));
   }

   private function computeSignature($msg, $secret)
   {
      return hash_hmac('sha256', $msg, $secret);
   }
}
