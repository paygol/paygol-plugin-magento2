<?php

namespace Paygol\PaygolMagento\Controller\Payment;

use Paygol\PaygolCore\PaygolApi;

class PaymentMethod extends \Magento\Framework\App\Action\Action
{
   /**
    * @var \Magento\Framework\Controller\Result\JsonFactory
    */
   protected $_resultJsonFactory;

   /**
    * @var \Psr\Log\LoggerInterface
    */
   protected $_logger;

   /**
    * @var \Paygol\PaygolMagento\Model\Factory\Connector
    */
   protected $_tpConnector;

    /**
    * @var \Magento\Checkout\Model\Session
    */
   protected $_checkoutSession;

   public function __construct(
      \Paygol\PaygolMagento\Model\Factory\Connector $tpc,
      \Magento\Framework\App\Action\Context $context,
      \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
      \Magento\Checkout\Model\Session $checkoutSession,
      \Psr\Log\LoggerInterface $logger
   ) {
      $this->_resultJsonFactory = $resultJsonFactory;
      $this->_logger = $logger;
      $this->_tpConnector = $tpc;
      $this->_checkoutSession = $checkoutSession;
      parent::__construct($context);
   }

   protected function _getCheckoutSession()
   {
      return $this->_checkoutSession;
   }

   public function execute()
   {
      $types = [];
      $error_message = '';
      try {
         $paygol = new PaygolApi(
            $this->_tpConnector->getTokenService(),
            $this->_tpConnector->getTokenSecret(),
            $this->_tpConnector->getEnviroment()
         );

         // Se obtiene el país desde la dirección de facturación del cliente (si no existe, se considera la dirección del despacho).
         $billing = $this->_checkoutSession->getQuote()->getBillingAddress();
         $shipping = $this->_checkoutSession->getQuote()->getShippingAddress();
         $country = empty($billing->getCountryId()) ?  $shipping->getCountryId(): $billing->getCountryId();

         $patmentMethods = $paygol->getPaymentMethods($country);
         $types = $patmentMethods['methods'];
      } catch (\Exception $exception) {
         $error_message = $exception->getMessage();
         $this->_logger->debug('Error ' . print_r($exception->getMessage(), true));
      }
      $result = $this->_resultJsonFactory->create();
      return $result->setData([
         'types' => $types,
         'billing' => $country, // Enviamos el código del país para efectos de debugging.
         'error_message' => $error_message,
      ]);
   }
}
