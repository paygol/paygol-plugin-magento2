<?php

namespace Paygol\PaygolMagento\Controller\Payment;

use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Sales\Model\Order\Payment\Transaction;
use Paygol\PaygolMagento\Observer\DataAssignObserver;

use Paygol\PaygolCore\PaygolApi;
use Paygol\PaygolCore\EnvironmentEnum;
use Paygol\PaygolCore\Models\Payer;
use Paygol\PaygolCore\Models\RedirectUrls;
use Paygol\PaygolCore\Exceptions\InvalidParameterException;

class Data extends \Magento\Framework\App\Action\Action
{
   /**
    * @var \Magento\Framework\UrlInterface
    */
   protected $_url;

   /**
    * @var \Magento\Framework\App\Config\ScopeConfigInterface
    */
   protected $_scopeConfig;

   /**
    * @var \Magento\Checkout\Model\Session
    */
   protected $_checkoutSession;

   /**
    * @var \Psr\Log\LoggerInterface
    */
   protected $_logger;

   /**
    * @var PaymentHelper
    */
   protected $_paymentHelper;

   /**
    * @var \Magento\Sales\Api\TransactionRepositoryInterface
    */
   protected $_transactionRepository;

   /**
    * @var \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface
    */
   protected $_transactionBuilder;

   /**
    * @var \Magento\Framework\Controller\Result\JsonFactory
    */
   protected $_resultJsonFactory;

   /**
    * @var \Paygol\PaygolMagento\Logger\Logger
    */
   protected $_pstPaygolLogger;

   /**
    * @var \Paygol\PaygolMagento\Model\Factory\Connector
    */
   protected $_tpConnector;

   public function __construct(
      \Paygol\PaygolMagento\Logger\Logger $pstPaygolLogger,
      \Paygol\PaygolMagento\Model\Factory\Connector $tpc,
      \Magento\Framework\App\Action\Context $context,
      \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
      \Magento\Checkout\Model\Session $checkoutSession,
      \Magento\Framework\Session\SessionManager $sessionManager,
      \Psr\Log\LoggerInterface $logger,
      PaymentHelper $paymentHelper,
      \Magento\Sales\Api\TransactionRepositoryInterface $transactionRepository,
      \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface $transactionBuilder,
      \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
   ) {
      parent::__construct($context);
      $this->_url = $context->getUrl();
      $this->_scopeConfig = $scopeConfig;
      $this->_checkoutSession = $checkoutSession;
      $this->_sessionManager = $sessionManager;
      $this->_logger = $logger;
      $this->_paymentHelper = $paymentHelper;
      $this->_transactionRepository = $transactionRepository;
      $this->_transactionBuilder = $transactionBuilder;
      $this->_resultJsonFactory = $resultJsonFactory;
      $this->_pstPaygolLogger = $pstPaygolLogger;
      $this->_tpConnector = $tpc;
   }

   protected function _getCheckoutSession()
   {
      return $this->_checkoutSession;
   }

   public function execute()
   {
      $url = '';
      $error_message = '';
      try {
         $order = $this->_getCheckoutSession()->getLastRealOrder();
         $payment = $order->getPayment();
         $subopt = $payment->getAdditionalInformation(DataAssignObserver::PAYMENT_METHOD_SUBOPT);
         $method = $payment->getMethod();
         $methodInstance = $this->_paymentHelper->getMethodInstance($method);
         $total = $methodInstance->getAmount($order);
         $response = $this->generateTransaction($order, $subopt, $total);
         $this->_logger->debug('Trx init result : ' . print_r($response, true));
         if (property_exists((object) $response, 'data')) {
            $url = $response->data['payment_method_url'];

            $payment = $order->getPayment();
            $payment->setTransactionId($response->data['transaction_id'])->setIsTransactionClosed(0);
            $payment->setParentTransactionId($order->getId());
            $payment->setIsTransactionPending(true);
            $payment->setSkipOrderProcessing(true);

            $transaction = $this->_transactionBuilder
               ->setPayment($payment)
               ->setOrder($order)
               ->setTransactionId($payment->getTransactionId())
               ->build(Transaction::TYPE_ORDER);

            $payment->addTransactionCommentsToOrder($transaction, __('Paygol Pending', 'paygol-magento'));
            $status = \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT;
            $state = \Magento\Sales\Model\Order::STATE_PENDING_PAYMENT;
            $order->setState($state)->setStatus($status);

            $payment->save();
            $order->save();
         } else {
            $error_message = __('Redirect url not exist. Please, check the country of payer.', 'paygol-magento');
         }
      } catch (\Exception $exception) {
         $this->_logger->debug('EXEPTION execute method : ' . print_r($exception->getMessage(), true));
         $this->_pstPaygolLogger->debug($exception->getMessage());
         $error_message = $exception->getMessage();
      }
      $result = $this->_resultJsonFactory->create();
      return $result->setData([
         'url' => $url,
         'error_message' => $error_message,
      ]);
   }

   public function generateTransaction($order, $subopt, $total)
   {
      $billing = $order->getBillingAddress();
      $shipping = $order->getShippingAddress();
      $country = empty($billing->getCountryId()) ?  $shipping->getCountryId(): $billing->getCountryId();
      $data = '';
      try {
         $paygol = new PaygolApi(
            $this->_tpConnector->getTokenService(),
            $this->_tpConnector->getTokenSecret(),
            $this->_tpConnector->getEnviroment()
         );

         $orderId = $order->getId();

         $redirectUrls = new RedirectUrls();
         $redirectUrls->setRedirects(
            $this->_url->getUrl('paygolmagento/payment/complete'),
            $this->_url->getUrl('paygolmagento/payment/complete') //'checkout/onepage/failure'
         );
         $paygol->setRedirects($redirectUrls);

         $payer = new Payer();
         $payer->setFirstName($billing->getFirstName());
         $payer->setLastName($billing->getLastName());
         $payer->setEmail($order->getCustomerEmail());
         $payer->setPhoneNumber($billing->getTelephone());
         $paygol->setPayer($payer);

         $paygol->setCountry($country);
         $paygol->setPrice($total, $order->getOrderCurrencyCode());
         $paygol->setPaymentMethod($subopt);
         $paygol->setName('Payment From Paygol');
         $paygol->setCustom($orderId);

         $this->_logger->info('Transaction : ' . print_r($paygol, true));
         $this->_logger->info('Método de pago : ' . $subopt);

         $data = (object) $paygol->createPayment();
      } catch (\Exception $exception) {
         $this->_logger->info('EXEPTION init Payment method : ' . print_r($exception->getMessage(), true));
         $this->_pstPaygolLogger->debug($exception->getMessage());
         throw new \Exception($exception->getMessage());
      }
      return $data;
   }
}
