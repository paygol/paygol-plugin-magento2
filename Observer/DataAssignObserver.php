<?php
namespace Paygol\PaygolMagento\Observer;

use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Framework\Event\Observer;
use Magento\Quote\Api\Data\PaymentInterface;


class DataAssignObserver extends AbstractDataAssignObserver
{
    const PAYMENT_METHOD_SUBOPT = 'payment_method_subopt';

    /**
     * @var array
     */
    protected $additionalInformationList = [
        self::PAYMENT_METHOD_SUBOPT,
    ];

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @var \Paygol\PaygolMagento\Logger\Logger
     */
    protected $_pstPaygolLogger;

    public function __construct(
        \Paygol\PaygolMagento\Logger\Logger $pstPaygolLogger,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->_logger = $logger;
        $this->_pstPaygolLogger = $pstPaygolLogger;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        try{
            $data = $this->readDataArgument($observer);
            $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
            if (!is_array($additionalData)) {
                return;
            }
            $paymentInfo = $this->readPaymentModelArgument($observer);
            foreach ($this->additionalInformationList as $additionalInformationKey) {
                if (isset($additionalData[$additionalInformationKey])) {
                    $paymentInfo->setAdditionalInformation(
                        $additionalInformationKey,
                        $additionalData[$additionalInformationKey]
                    );
                }
            }
        } catch (\Exception $exception){
            $this->_pstPaygolLogger->debug($exception->getMessage());
        }
    }
}