<?php
namespace Paygol\PaygolMagento\Observer;

use Magento\Framework\Event\ObserverInterface;

class AfterPlaceOrder implements ObserverInterface
{
   protected $_quoteFactory;
   protected $_logger;

   public function __construct(\Magento\Quote\Model\QuoteFactory $quoteFactory, \Psr\Log\LoggerInterface $logger)
   {
      $this->_quoteFactory = $quoteFactory;
      $this->_logger = $logger;
   }

   /**
    *
    * @param \Magento\Framework\Event\Observer $observer
    * @return void
    */
   public function execute(\Magento\Framework\Event\Observer $observer)
   {
      try {
         $order = $observer->getEvent()->getOrder();

         // Activate cart if order is canceled.
         if ($order->getState() === \Magento\Sales\Model\Order::STATE_CANCELED) {
            $quote = $this->_quoteFactory->create()->load($order->getQuoteId());
            $quote->setIsActive(true)->save();
         }
      } catch (\Exception $exception) {
         $this->_logger->info('EXEPTION AfterPlaceOrder : ' . print_r($exception->getMessage(), true));
      }

      return $this;
   }
}
