define([
    "jquery",
    "ko",
    "Magento_Checkout/js/view/payment/default",
    "Magento_Checkout/js/action/place-order",
    "Magento_Checkout/js/action/select-payment-method",
    "Magento_Customer/js/model/customer",
    "Magento_Checkout/js/checkout-data",
    "mage/url",
], function (
    $,
    ko,
    Component,
    placeOrderAction,
    selectPaymentMethodAction,
    customer,
    checkoutData,
    url
) {
    "use strict";
    return Component.extend({
        defaults: {
            template: "Paygol_PaygolMagento/payment/paygolmagento",
        },
        /** @inheritdoc */
        initialize: function () {
            this._super();
            this.metodosdepago = ko.observableArray([]);
            this.selectedPaymentCode = ko.observable();
            // Search pay Methods
            var _this = this;
            $("body").loader("show");
            $.get(
                url.build("paygolmagento/payment/paymentmethod"),
                function (data) {
                    if ( data && data.error_message && data.error_message !== "" ) {
                        alert(data.error_message);
                    } else if (data && data.types) {
                        Object.keys(data.types).forEach(key => {
                                _this.metodosdepago.push({
                                    code: data.types[key].code,
                                    name: data.types[key].name,
                                    image: data.types[key].image_164x64,
                                });
                        });
                    }
                    $("body").loader("hide");
                }
            );
        },
        
        clickPaymentMethod(code) {
            let $elem = $("input[value='" + code + "']");
            if ($elem.length > 0) {
                $elem.attr('checked', true);
                this.selectPaymentMethod(code);
            }
        },

        selectPaymentMethod: function (code) {
            this.selectedPaymentCode(code);
            selectPaymentMethodAction(this.getData());
            checkoutData.setSelectedPaymentMethod(this.item.method)
            return true;
        },

        placeOrder: function (data, event) {
            if (event) {
                event.preventDefault();
            }
            var self = this,
                placeOrder,
                emailValidationResult = customer.isLoggedIn(),
                loginFormSelector = "form[data-role=email-with-possible-login]";

            if (!customer.isLoggedIn()) {
                $(loginFormSelector).validation();
                emailValidationResult = Boolean($(loginFormSelector + " input[name=username]").valid());
            }

            if (emailValidationResult && this.validate()) {
                this.isPlaceOrderActionAllowed(false);
                placeOrder = placeOrderAction(this.getData(), false, this.messageContainer);

                $.when(placeOrder).done(function () {
                    self.isPlaceOrderActionAllowed(true);
                }).done(this.afterPlaceOrder.bind(this));

                console.info("place order TRUE");
                return true;
            }
            console.info("place order FALSE");
            return false;
        },

        afterPlaceOrder: function () {
            console.log("afterPlaceOrder");
            $('body').loader('show');
            $.get(url.build("paygolmagento/payment/data"), function (data) {
                if (data.error_message && data.error_message !== "") {
                    alert(data.error_message);
                } else if (data.url) {
                    window.location = data.url;
                }
                $('body').loader('hide');
            });
        },

        getLogoUrl: function () {
            return window.checkoutConfig.logoUrl;
        },

        getData: function () {
            let code = this.selectedPaymentCode();
            let data = {
                'method': this.getCode(),
                'additional_data': {
                    'payment_method_subopt': code,
                },
            };
            return data;
        },

        isOptionActive(code) {
            return code == this.selectedPaymentCode();
        }
    });
});
