define([
    "uiComponent",
    "Magento_Checkout/js/model/payment/renderer-list",
], function (Component, rendererList) {
    "use strict";
    rendererList.push({
        type: "paygolmagento",
        component:
            "Paygol_PaygolMagento/js/view/payment/method-renderer/paygolmagento",
    });
    /** Add view logic here if needed */
    return Component.extend({});
});
