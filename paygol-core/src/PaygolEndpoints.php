<?php

namespace Paygol\PaygolCore;

use Paygol\PaygolCore\EnvironmentEnum;
use Paygol\PaygolCore\Exceptions\InvalidEnviromentException;

abstract class EndpointEnum
{
   const API_PRO_BASE_URL = 'https://www.paygol.com/api/v2/';
   const API_DEV_BASE_URL = 'https://apinewqa.paygol.com/api/v2/';

   const API_PATH_AUTH_TOKEN = 'auth/token';
   const API_PATH_PAYMENT_METHODS = 'payment/methods';
   const API_PATH_PAYMENT_STATUS = 'payment/status';
   const API_PATH_PAYMENT_CREATE = 'payment/create';
}

class PaygolEndpoints
{
   const ERROR_VALUE_INVALID = 'Invalid Enviroment';

   /**
    * Enviroment mode
    *
    * @var string
    */
   protected $enviroment;

   /**
    * @param string $enviroment
    */
   public function __construct($enviroment)
   {
      $this->enviroment = $enviroment;
   }

   /**
    * Obtiene url de login token jwt segun ambiente activado
    */
   public function getLoginUrl()
   {
      if ($this->enviroment == EnvironmentEnum::PRODUCTION) {
         return EndpointEnum::API_PRO_BASE_URL . EndpointEnum::API_PATH_AUTH_TOKEN;
      } elseif ($this->enviroment == EnvironmentEnum::DEVELOPMENT) {
         return EndpointEnum::API_DEV_BASE_URL . EndpointEnum::API_PATH_AUTH_TOKEN;
      }
      throw new InvalidEnviromentException(self::ERROR_VALUE_INVALID);
   }

   /**
    * Obtiene url de metodo de pago segun ambiente activado
    */
   public function getPaymentMethodsUrl()
   {
      if ($this->enviroment == EnvironmentEnum::PRODUCTION) {
         return EndpointEnum::API_PRO_BASE_URL . EndpointEnum::API_PATH_PAYMENT_METHODS;
      } elseif ($this->enviroment == EnvironmentEnum::DEVELOPMENT) {
         return EndpointEnum::API_DEV_BASE_URL . EndpointEnum::API_PATH_PAYMENT_METHODS;
      }
      throw new InvalidEnviromentException(self::ERROR_VALUE_INVALID);
   }

   /**
    * Obtiene url de status transaction segun ambiente activado
    */
   public function getPaymentStatusUrl()
   {
      if ($this->enviroment == EnvironmentEnum::PRODUCTION) {
         return EndpointEnum::API_PRO_BASE_URL . EndpointEnum::API_PATH_PAYMENT_STATUS;
      } elseif ($this->enviroment == EnvironmentEnum::DEVELOPMENT) {
         return EndpointEnum::API_DEV_BASE_URL . EndpointEnum::API_PATH_PAYMENT_STATUS;
      }
      throw new InvalidEnviromentException(self::ERROR_VALUE_INVALID);
   }

   /**
    * Obtiene url de pay transaction segun ambiente activado
    */
   public function getCreatePaymentUrl()
   {
      if ($this->enviroment == EnvironmentEnum::PRODUCTION) {
         return EndpointEnum::API_PRO_BASE_URL . EndpointEnum::API_PATH_PAYMENT_CREATE;
      } elseif ($this->enviroment == EnvironmentEnum::DEVELOPMENT) {
         return EndpointEnum::API_DEV_BASE_URL . EndpointEnum::API_PATH_PAYMENT_CREATE;
      }
      throw new InvalidEnviromentException(self::ERROR_VALUE_INVALID);
   }
}
