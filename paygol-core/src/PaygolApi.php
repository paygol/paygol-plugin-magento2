<?php

namespace Paygol\PaygolCore;

use Paygol\PaygolCore\Exceptions\InvalidParameterException;
use Paygol\PaygolCore\Exceptions\InvalidSignatureException;

use Paygol\PaygolCore\Models\Payer;
use Paygol\PaygolCore\Models\RedirectUrls;

use Paygol\PaygolCore\PaygolEndpoints;

class PaygolApi
{
   const API_IMPL = 'php/1.0';

   const API_PATH_PAYMENT_CREATE = 'payment/create';

   const MODE_API = 'api';

   /**
    * Return as JSON data
    *
    * @var boolean
    */
   protected $asJSON = false;

   /**
    * Transaction mode
    *
    * @var string
    */
   protected $pg_mode;

   /**
    * Merchant service ID
    *
    * @var int
    */
   protected $service_id;

   /**
    * Merchant shared secret
    *
    * @var string
    */
   protected $shared_secret;

   /**
    * Token
    *
    * @var string
    */
   protected $token;

   /**
    * Currency (ISO 4217)
    *
    * @var string
    */
   protected $pg_currency = 'null';

   /**
    * Price
    *
    * @var float
    */
   protected $pg_price = null;

   /**
    * Country (ISO 3166-2)
    *
    * @var string
    */
   protected $pg_country = 'CL';

   /**
    * Language
    *
    * @var string
    */
   protected $pg_language = 'es';

   /**
    * Payment method code
    *
    * @var string
    */
   protected $pg_method = null;

   /**
    * @var array
    */
   protected $payer = [];

   /**
    * @var array
    */
   protected $redirect_urls = [];

   /**
    * @var mixed
    */
   protected $pg_sub_merchant_id = null;

   /**
    * @var string
    */
   protected $pg_sub_merchant_url = null;

   /**
    * @var mixed
    */
   protected $pg_custom = null;

   /**
    * @var mixed
    */
   protected $pg_name = null;

   /**
    * Success return URL
    *
    * @var string
    */
   protected $pg_return_url = null;

   /**
    * Fail return URL
    *
    * @var string
    */
   protected $pg_cancel_url = null;

   /**
    * Endponit enviroment
    *
    * @var paygolEndpoints
    */
   protected $paygolEndpoints;
   /**
    * @param int $service_id
    * @param string $shared_secret
    * @param EnvironmentEnum $enviroment
    */
   public function __construct($service_id, $shared_secret, $enviroment)
   {
      $this->service_id = $service_id;
      $this->shared_secret = $shared_secret;
      $this->pg_mode = self::MODE_API;
      $this->token = null;
      $this->paygolEndpoints = new PaygolEndpoints($enviroment);
      $this->auth();
   }

   public function returnJSON($asJSON = true)
   {
      $this->asJSON = $asJSON;
   }

   /**
    * Set submerchant Info
    *
    * @param mixed $id
    * @param string $url
    *
    * @return void
    */
   public function setSubmerchantInfo($id, $url)
   {
      $this->pg_sub_merchant_id = $id;
      $this->pg_sub_merchant_url = $url;
   }

   /**
    * Set custom info
    *
    * @param string $custom
    *
    * @return void
    */
   public function setCustom($custom)
   {
      $this->pg_custom = $custom;
   }

   /**
    * @param string $name
    *
    * @return void
    */
   public function setName($name)
   {
      $this->pg_name = $name;
   }

   /**
    * Get payment methods
    *
    * @param string $country
    *
    * @return array
    */
   public function getPaymentMethods($country)
   {
      $ret = $this->get($this->paygolEndpoints->getPaymentMethodsUrl(), ['pg_country' => strtoupper($country)]);
      self::expect_vars($ret, ['methods']);

      return $this->format_response($ret);
   }

   /**
    * Get payment status
    *
    * @param string $id
    *
    * @return array
    */
   public function getPaymentStatus($id)
   {
      $ret = $this->get($this->paygolEndpoints->getPaymentStatusUrl(), ['transaction_id' => $id]);
      // self::expect_vars($ret, ["payment"]);

      if (!isset($ret['payment']) && !isset($ret['error'])) {
         throw new \Exception('Invalid response data');
      }

      return $this->format_response($ret);
   }

   /**
    * Create payment
    *
    * @return array
    */
   public function createPayment()
   {
      $args = [
         'pg_currency' => $this->pg_currency,
         'pg_price' => $this->pg_price,
         'pg_country' => $this->pg_country,
         'pg_language' => $this->pg_language,
         'pg_mode' => $this->pg_mode,
         'pg_method' => $this->pg_method,
         'pg_format' => 'json',
         'pg_first_name' => null,
         'pg_last_name' => null,
         'pg_email' => null,
         'pg_phone' => null,
         // Este se dato se comenta, ya que está en duda su uso en la API
         // 'pg_personalid' => null,
      ];

      $args = array_merge($args, $this->payer, $this->redirect_urls);

      if ($this->pg_sub_merchant_id != null) {
         $args['pg_sub_merchant_id'] = $this->pg_sub_merchant_id;
      }

      if ($this->pg_sub_merchant_url != null) {
         $args['pg_sub_merchant_url'] = $this->pg_sub_merchant_url;
      }

      if ($this->pg_custom != null) {
         $args['pg_custom'] = $this->pg_custom;
      }

      if ($this->pg_name != null) {
         $args['pg_name'] = $this->pg_name;
      }

      foreach ($args as $k => $v) {
         if (null == $v) {
            throw new InvalidParameterException("Parameter {$k} is requiered");
         }
      }

      $ret = $this->get($this->paygolEndpoints->getCreatePaymentUrl(), $args);

      if (!isset($ret['data']) && !isset($ret['error'])) {
         throw new \Exception('Invalid response data');
      }

      return $this->format_response($ret);
   }

   /**
    * Set payment method
    *
    * @param string $method
    *
    * @return void
    */
   public function setPaymentMethod($method)
   {
      $this->pg_method = $method;
   }

   /**
    * Set country
    *
    * @param string $country
    *
    * @return void
    */
   public function setCountry($country)
   {
      $this->pg_country = $country;
   }

   /**
    * Set price
    *
    * @param float $price
    * @param string $currency
    *
    * @return void
    */
   public function setPrice($price, $currency = 'CLP')
   {
      $this->pg_price = $price;
      $this->pg_currency = $currency;
   }

   public function setPayer(Payer $payer)
   {
      $this->payer = $payer->asArray();
   }

   /**
    * @param Payer $redirect_urls
    * @return void
    */
   public function setRedirects(RedirectUrls $redirect_urls)
   {
      $this->redirect_urls = $redirect_urls->asArray();
   }

   public function getToken()
   {
      return $this->token;
   }

   /**
    * Authenticate user
    *
    * @return void
    */
   protected function auth()
   {
      // $ret = $this->get($this->paygolEndpoints->getLoginUrl());
      // self::expect_vars($ret, ['token']);
      $this->token = 'ee977806d7286510da8b9a7492ba58e2484c0ecc'; // $ret['token'];
   }

   /**
    * @param array $o
    * @param array $vars
    *
    * @throws Exception
    *
    * @return void
    */
   protected static function expect_vars($o, $vars)
   {
      foreach ($vars as $v) {
         if (!isset($o[$v])) {
            throw new \Exception("Missing var $v");
         }
      }
   }

   /**
    * Compute signature
    *
    * @param string $msg
    * @param string $secret
    *
    * @return string
    */
   protected static function compute_signature($msg, $secret)
   {
      return hash_hmac('sha256', $msg, $secret);
   }

   /**
    * Validate response
    *
    * @param string $json_response
    * @param array $headers
    *
    * @throws Exception
    *
    * @return array
    */
    protected function validate_response($json_response, $headers)
    {
       $result = json_decode($json_response, true);
 
       if (json_last_error() != JSON_ERROR_NONE) {
          throw new \Exception('Error to parse json response');
       }
 
       $sig_response = self::compute_signature($json_response, $this->shared_secret);
 
       if (!isset($headers['X-PG-SIG'])) {
          throw new InvalidSignatureException('Signature header does not exist');
       }
 
       if (trim($sig_response) !== trim($headers['X-PG-SIG'])) {
          throw new InvalidSignatureException("Invalid signature: {$sig_response} {$headers['X-PG-SIG']}");
       }
 
       self::expect_vars($result, ['result']);
 
       return $result;
    }

   /**
    * @param string $url
    * @param array $args
    *
    * @throws Exception
    *
    * @return array
    */
    protected function get($url, $args = [])
    {
       $args['pg_serviceid'] = $this->service_id;
 
       if ($this->token != null) {
          $args['pg_token'] = $this->token;
       }
 
       $json_request = json_encode($args, JSON_FORCE_OBJECT | JSON_PRESERVE_ZERO_FRACTION);
       $sig_request = self::compute_signature($json_request, $this->shared_secret);
 
       try {
          $curl = curl_init();
 
          curl_setopt_array($curl, array(
             CURLOPT_URL => $url,
             CURLOPT_RETURNTRANSFER => true,
             CURLOPT_ENCODING => '',
             CURLOPT_MAXREDIRS => 10,
             CURLOPT_TIMEOUT => 0,
             CURLOPT_FOLLOWLOCATION => true,
             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
             CURLOPT_CUSTOMREQUEST => 'POST',
             CURLOPT_POSTFIELDS => $json_request,
             CURLOPT_HEADER => true,
             CURLOPT_HTTPHEADER => array(
                'X-PG-SIG: ' . $sig_request,
                'Content-Type: application/json'
             ),
          ));
 
          $response = curl_exec($curl);
 
          // how big are the headers
          $headerSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
          $headerStr = substr($response, 0, $headerSize);
          $bodyStr = substr($response, $headerSize);
 
          // convert headers to array
          $headers = $this->headersToArray($headerStr);
 
          curl_close($curl);
 
          return $this->validate_response($bodyStr, $headers);
       } catch (\Exception $e) {
          throw $e;
       }
    }
 
    private function headersToArray($str)
    {
       $headers = array();
       $headersTmpArray = explode("\r\n", $str);
       for ($i = 0; $i < count($headersTmpArray); ++$i) {
          // we dont care about the two \r\n lines at the end of the headers
          if (strlen($headersTmpArray[$i]) > 0) {
             // the headers start with HTTP status codes, which do not contain a colon so we can filter them out too
             if (strpos($headersTmpArray[$i], ":")) {
                $headerName = substr($headersTmpArray[$i], 0, strpos($headersTmpArray[$i], ":"));
                $headerValue = substr($headersTmpArray[$i], strpos($headersTmpArray[$i], ":") + 1);
                $headers[$headerName] = $headerValue;
             }
          }
       }
       return $headers;
    }

   protected function format_response($data)
   {
      return $this->asJSON ? json_encode($data) : $data;
   }
}
