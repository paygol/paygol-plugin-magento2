<?php

namespace Paygol\PaygolCore;

abstract class EnvironmentEnum
{
   const DEVELOPMENT = 'DEVELOPMENT';
   const PRODUCTION = 'PRODUCTION';

   public static function isValid($value)
   {
      return $value == EnvironmentEnum::DEVELOPMENT || $value == EnvironmentEnum::PRODUCTION;
   }
}
