<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require './src/bootstrap.php';

use Paygol\PaygolCore\PaygolApi;
use Paygol\PaygolCore\EnvironmentEnum;
use Paygol\PaygolCore\Models\Payer;
use Paygol\PaygolCore\Models\RedirectUrls;
use Paygol\PaygolCore\Exceptions\InvalidParameterException;

/** KDU Credentials */
const SERVICE_ID = 479051;
const SECRET_KEY = 'cae9af83-61d4-11ec-8b6b-02ec38304cd8';

/* Paygol credentials */
//const SERVICE_ID = 477980;
//const SECRET_KEY = '8edb5be2-e479-11e9-9c67-128b57940774';

// PROD
//$paygol = new PaygolApi(SERVICE_ID, SECRET_KEY, EnvironmentEnum::PRODUCTION);

// DEV
$paygol = new PaygolApi(SERVICE_ID, SECRET_KEY, EnvironmentEnum::DEVELOPMENT);

//echo $paygol->getToken();

try {
   $redirectUrls = new RedirectUrls();
   $redirectUrls->setRedirects('https://www.my-site.com/success', 'https://www.my-site.com/failure'); // optional
   $paygol->setRedirects($redirectUrls);

   $payer = new Payer();
   $payer->setFirstName('John');
   $payer->setLastName('Doe');
   $payer->setEmail('jdoe@my-site.com');
   $paygol->setPayer($payer);

   $paygol->setCountry('CL');
   $paygol->setPrice(8500, 'CLP');
   $paygol->setPaymentMethod('khipu');
   $paygol->setName('Payment From Paygol');
   $paygol->setCustom(3);

   $payment = $paygol->createPayment();
   var_dump($payment);

   //var_dump($paygol->getPaymentMethods('CL'));

   //echo $payment['data']['payment_method_url'];

   if (!empty($payment['data']['payment_method_url'])) {
      // do something
   }
} catch (InvalidParameterException $e) {
   die($e->getMessage());
} catch (\Exception $e) {
   die($e->getMessage());
}
