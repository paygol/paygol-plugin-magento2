<?php

namespace Paygol\PaygolMagento\Logger;


class Handler extends \Magento\Framework\Logger\Handler\Base
{
    protected $fileName = '/var/log/paygolmagento/info.log';
    protected $loggerType = \Monolog\Logger::INFO;
}